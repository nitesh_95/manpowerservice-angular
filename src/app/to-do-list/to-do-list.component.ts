import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.less']
})
export class ToDoListComponent implements OnInit {
date:any
todotask:any
employeedetailslist:any = []
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getalllist(event)
  }
todoList(event){
  const base_URL = 'http://localhost:8991/save'
  this.http.post(base_URL,{
      date:this.date,
      todotask:this.todotask
  }).subscribe(data=>{
    if(data['status'] == '00'){
      console.log(data)
      alert("To Do List has been added Successfully");
      window.location.reload()

    }

  })
  
}
getalllist(event){

 const base_URL = 'http://localhost:8991/getAll'
  this.http.post(base_URL,{
  }).subscribe(data=>{
    this.employeedetailslist.push(data)
    this.employeedetailslist = this.employeedetailslist[0]
      console.log(data)
  })
}
delete(event){
  var selected_id =event.currentTarget.id
  alert("Are you sure you want to delete the Data??")
  const base_URL = 'http://localhost:8991/delete/'+ selected_id
  this.http.post(base_URL,{}).subscribe(data =>{
    console.log(data)
    if (data['status'] == '00') {
      alert("Data has been sucessfully deleted")
      window.location.reload();
    }
  })
  }
}