import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { SalaryManagementComponent } from './salary-management/salary-management.component';
import { SliderComponent } from './slider/slider.component';
import { SalaryListComponent } from './salary-list/salary-list.component';
import { MonthlysalaryComponent } from './monthlysalary/monthlysalary.component';
import { ViewpayslipComponent } from './viewpayslip/viewpayslip.component';
import { PayslipAugustComponent } from './payslip-august/payslip-august.component';
import { PrintpayslipComponent } from './printpayslip/printpayslip.component';
import { UploaddocComponent } from './uploaddoc/uploaddoc.component';
import { FileuploadViewComponent } from './fileupload-view/fileupload-view.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'slider', component: SliderComponent },
  { path: 'home', component: HomeComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'employeedetails', component: EmployeedetailsComponent },
  { path: 'todo', component: ToDoListComponent },
  { path: 'salary', component: SalaryManagementComponent },
  { path: 'salaryDetails', component: SalaryListComponent },
  { path: 'monthlysalary', component: MonthlysalaryComponent },
  { path: 'payslip', component: ViewpayslipComponent },
  { path: 'payslipAugust', component: PayslipAugustComponent },
  { path: 'printInvoice', component: PrintpayslipComponent },
  { path: 'uploaddoc', component: UploaddocComponent },
  { path: 'uploadView', component: FileuploadViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }