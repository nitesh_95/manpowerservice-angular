import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintpayslipComponent } from './printpayslip.component';

describe('PrintpayslipComponent', () => {
  let component: PrintpayslipComponent;
  let fixture: ComponentFixture<PrintpayslipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintpayslipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintpayslipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
