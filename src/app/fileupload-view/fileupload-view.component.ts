import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fileupload-view',
  templateUrl: './fileupload-view.component.html',
  styleUrls: ['./fileupload-view.component.less']
})
export class FileuploadViewComponent implements OnInit {

  employeedetailsList:any = []
  searchText;
  p: number = 1;
  pendingCustomerData = [];
  public pageSize: number = 5;
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getAllEmployee(event)
  }

  getAllEmployee(event){
  
    const base_URL = 'http://localhost:8993/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)
  
    })
  }
  }