import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-employeedetails',
  templateUrl: './employeedetails.component.html',
  styleUrls: ['./employeedetails.component.less']
})

export class EmployeedetailsComponent implements OnInit {
  approveCustomerData: any = []
  joineeddates:any
  
  name: any
  adhar: any
  mobile: any
  address: any
  employeedetailsList: any = [];
  pan: any
  id: any
  updatetargetId: any
  names: any
  adhars: any
  mobiles: any
  addresses: any
  pans: any
  empid: any
  empids:any
  joinedDate:any
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.getAllList(event)
  }
  clickSubmit(event) {
    const base_URL = 'http://localhost:8990/save'
    this.http.post(base_URL, {
      id: this.empid,
      name: this.name,
      adhar: this.adhar,
      pan: this.pan,
      mobile: this.mobile,
      address: this.address,
      joineddate:this.joinedDate
    }).subscribe(data => {
      console.log(this.empid)
      console.log(data['status'])
      if (data['status'] == '00') {
        alert("Employee has been Added Successfully")
        window.location.reload();
      }

    })


  }
  getAllList(event) {

    const base_URL = 'http://localhost:8990/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {

      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
  fetchData(event) {
    alert("welcome")

    var targetId = event.targetId.id
    console.log(targetId)
    this.employeedetailsList.forEach(data => {
      if (targetId == data.applicationid) {
        console.log(data.applicationid)

        this.approveCustomerData.push(data)
        this.id = data.applicationid

      }
    })
  }
  editedDetails(event) {
    var selected_id = event.currentTarget.id
    const base_URL = 'http://localhost:8990/getEmployee/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.empids=data['id']
      console.log(this.empids)
      this.names = data['name']
      this.adhars = data['adhar']
      this.pans = data['pan']
      this.mobiles = data['mobile']
      this.addresses = data['address']
      this.joineeddates=data['joineddate']

    })
  }
  // editedDetails(event) {
  //   const base_URL = 'http://localhost:8990/updateData'
  //   this.http.post(base_URL, {
  //     id: this.id,
  //     name: this.name,
  //     adhar: this.adhar,
  //     pan: this.pan,
  //     mobile: this.mobile,
  //     address: this.address
  //   }).subscribe(data => {
  //     console.log(data['status'])
  //     if (data['status'] == '00') {
  //       alert("Employee has been Added Successfully")
  //       window.location.reload();
  //     }

  //   })
  // }
  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are You Sure you want to Delete Data ??")
    const base_URL = 'http://localhost:8990/delete/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      console.log(data['status'])
      if (data['status'] == '00') {
        alert("Employee has been Deleted Successfully")
        window.location.reload();
      }

    })
  }
}