import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpayslipComponent } from './viewpayslip.component';

describe('ViewpayslipComponent', () => {
  let component: ViewpayslipComponent;
  let fixture: ComponentFixture<ViewpayslipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpayslipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpayslipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
