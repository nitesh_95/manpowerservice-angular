import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ViewApplicationServiceService } from '../view-application-service.service';

@Component({
  selector: 'app-viewpayslip',
  templateUrl: './viewpayslip.component.html',
  styleUrls: ['./viewpayslip.component.less']
})
export class ViewpayslipComponent implements OnInit {

  employeedetailsLists2: any = []
  employeedetailsLists3: any = []
  employeedetailsLists4: any = []
  employeedetailsLists5: any = []
  employeedetailsLists1: any = []
  employeeData: any
  constructor(private http: HttpClient, private router: Router, private viewApplication: ViewApplicationServiceService) { }
  pageSize: any = 10
  ngOnInit() {
    this.getAllList1(event)
    this.getAllList2(event)
    this.getAllList3(event)
    this.getAllList3(event)
    this.getAllList4(event)
    this.getAllList5(event)

  }
  getAllList1(event) {
    const base_URL = 'http://localhost:8996/getAll/august'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists1.push(data)
      this.employeedetailsLists1 = this.employeedetailsLists1[0]
      console.log(data)

    })
  }
  getAllList2(event) {
    const base_URL = 'http://localhost:8996/getAll/september'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists2.push(data)
      this.employeedetailsLists2 = this.employeedetailsLists2[0]
      console.log(data)

    })
  }
  getAllList3(event) {
    const base_URL = 'http://localhost:8996/getAll/october'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists3.push(data)
      this.employeedetailsLists3 = this.employeedetailsLists3[0]
      console.log(data)

    })
  }
  getAllList4(event) {
    const base_URL = 'http://localhost:8996/getAll/november'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists4.push(data)
      this.employeedetailsLists4 = this.employeedetailsLists4[0]
      console.log(data)

    })
  }

  getAllList5(event) {
    const base_URL = 'http://localhost:8996/getAll/december'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists5.push(data)
      this.employeedetailsLists5 = this.employeedetailsLists5[0]
      console.log(data)

    })
  }
  fetchData(event) {
    this.employeeData = []
    var selected_id = event.currentTarget.id
    this.employeedetailsLists1.forEach(data => {
      // if (selected_id == data.id) {
      this.employeeData.push(data)
      this.router.navigate(['/printInvoice']);
      // }
    })
  }
}  