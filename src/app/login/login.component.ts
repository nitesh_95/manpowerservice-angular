import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AppComponent } from '../app.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent  {


   username:any
  password: any;
  invalidLogin = false

  common_IP: any;
  constructor(private router: Router,
    private http: HttpClient ) { }

  checkLogin(event) {
    this.username = event.target.elements[0].value;
    sessionStorage.setItem('username', this.username)
    this.password = event.target.elements[1].value;
    sessionStorage.setItem('password', this.password)
    if (this.username == '') {
      alert('please enter user ID')
    } else if (this.password == '') {
      alert('please enter password')
    } else {
      const base_URL = 'http://localhost:8995/login?UserId=' + this.username + '&Password=' + this.password;
      return this.http.post(base_URL, {}).subscribe(data => {
        if (data['status'] == '00') {
         alert("Welcome to Ganga Manpower Services")
         this.router.navigate(['dashboard'])
      
        } else {
          alert('please Enter valid credintials')
        }
      })
    }

  }
  
}
