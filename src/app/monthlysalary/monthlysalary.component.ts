import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ViewApplicationServiceService } from '../view-application-service.service';
import * as $ from "jquery"

@Component({
  selector: 'app-monthlysalary',
  templateUrl: './monthlysalary.component.html',
  styleUrls: ['./monthlysalary.component.less']
})
export class MonthlysalaryComponent implements OnInit {
  id: any
  empname: any
  checkinTemp:any
  salary: any
  month: any
  adhar: any
  myarrays:any=[]
  days: any
  previousdaysalary: any
  perdaysalary: any
  outcomesalary: any
  julysalary: any
  augustsalary: any
  sepsalary: any
  octsalary: any
  novsalary: any
  paymentstatus: any = 'Paid'
  decsalary: any
  searchText;
  status: any
  salarymonth: any
  p: number = 1;
  totaldays: any
  workingdays: any
  casualleaves: any
  sickleave: any
  employeeData = [];
  workingDays: any
  salarymonths: any
  monthlydays: any
  monthlysalary: any
  dateofpayment: any
  monthlysalarys: any
  monthdays: any
  pendingCustomerData = [];
  employeedetailsList: any = []
  employeedetailsLists1: any = []
  employeedetailsLists2: any = []
  employeedetailsLists3: any = []
  employeedetailsLists4: any = []
  employeedetailsLists5: any = []
  public pageSize: number = 5;
  disabledAgreement: boolean = true;
  checked: boolean = false
  myarray: any = []
  constructor(private http: HttpClient, private router: Router, private viewApplication: ViewApplicationServiceService) { }

  ngOnInit() {
    this.getAllList(event)
    this.getAllList1(event)
    this.getAllList2(event)
    this.getAllList3(event)
    this.getAllList4(event)
    this.getAllList5(event)
  }

  calculator(event) {
    this.workingDays = $("#workingDays").val();
    this.salarymonths = $("#salarymonth").val();
    this.salary = $("#salary").val();
    var m = parseInt(this.salarymonths)
    var p = parseInt(this.workingDays);
    var s = parseInt(this.salary)
    console.log(s)
    console.log(m)
    console.log(p)
    var e = (s / m) * p;
    var finalamount = Math.round(e);
    $("#outcomesalary").val(Math.round(e));
    console.log(finalamount)
  }
  getAllList1(event) {
    const base_URL = 'http://localhost:8996/getAll/august'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists1.push(data)
      this.viewApplication.employeeAugust(this.employeedetailsLists1)
      this.employeedetailsLists1 = this.employeedetailsLists1[0]
      console.log(data)

    })
  }
  getAllList(event) {
    const base_URL = 'http://localhost:8996/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
  getAllList2(event) {
    const base_URL = 'http://localhost:8996/getAll/september'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists2.push(data)
      this.viewApplication.employeeSeptember(this.employeedetailsLists2)
      this.employeedetailsLists2 = this.employeedetailsLists2[0]
      console.log(data)

    })
  }
  getAllList3(event) {
    const base_URL = 'http://localhost:8996/getAll/october'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists3.push(data)
      this.viewApplication.employeeOctober(this.employeedetailsLists3)
      this.employeedetailsLists3 = this.employeedetailsLists3[0]
      console.log(data)

    })
  }
  getAllList4(event) {
    const base_URL = 'http://localhost:8996/getAll/november'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists4.push(data)
      this.viewApplication.employeeNovember(this.employeedetailsLists4)
      this.employeedetailsLists4 = this.employeedetailsLists4[0]
      console.log(data)

    })
  }
  getAllList5(event) {
    const base_URL = 'http://localhost:8996/getAll/december'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsLists5.push(data)
      this.viewApplication.employeeDecember(this.employeedetailsLists5)
      this.employeedetailsLists5 = this.employeedetailsLists5[0]
      console.log(data)

    })
  }
  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are you sure you want to delete the Data??")
    const base_URL = 'http://localhost:8996/delete/' + selected_id
    this.http.post(base_URL, {}).subscribe(data => {
      console.log(data)
      if (data['status'] == '00') {
        alert("Data has been sucessfully deleted")
        window.location.reload();
      }
    })
  }
  fetchData(event) {
    this.employeeData = []
    var selected_id = event.currentTarget.id
    this.employeedetailsLists1.forEach(data => {
      // if (selected_id == data.id) {
      this.employeeData.push(data)
      this.viewApplication.employeeAugust(this.employeeData)
      this.router.navigate(['/payslip']);
      // }
    })
  }
  getempsbyid(event) {

  }

  getempbyid(event) {

    var selected_id = event.currentTarget.id
    this.disabledAgreement = !event.target.checked;
    this.workingDays = $("#workingDays" + selected_id).val();
    this.salarymonths = $("#salarymonth" + selected_id).val();
    this.sickleave = $("#sickleave" + selected_id).val();
    this.casualleaves = $("#casualleaves" + selected_id).val();
    this.dateofpayment = $("#dateofpayment" + selected_id).val();
    console.log(this.workingDays, this.salarymonth, this.sickleave, this.casualleaves, this.dateofpayment)
    const base_URL = 'http://localhost:8996/getEmployee/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.id = data['id']
      this.empname = data['empname']
      this.monthlysalarys = data['monthlysalary']
    })
  }
  onchange(event) {
    var salarymonth = (<HTMLInputElement>document.getElementById("months")).value;
    var test = salarymonth.split(',')
    this.monthdays = test[0]
    this.salarymonth = test[1]
  }


  deleteFieldValue(index) {
    this.employeedetailsList.splice(index, 1);
  }



  clickSubmit(event) {
    let myarray = [
      {
        id: selected_id, emp: this.empname, monthlysalary: this.monthlysalarys, sickleave: this.sickleave,
        casualleave: this.casualleaves, dateofpayment: this.dateofpayment, monthdays: this.monthlydays
        , workingdays: this.workingDays, outcomesalary: this.outcomesalary
      }
    ];

    var m = parseInt(this.monthdays)
    var p = parseInt(this.workingDays);
    var s = parseInt(this.monthlysalarys)
    console.log(s)
    console.log(m)
    console.log(p)
    var e = (s / m) * p;
    var finalamount = Math.round(e);
    $("#outcomesalary").val(Math.round(e));
    console.log(finalamount)


    var selected_id = event.currentTarget.id
    console.log(this.salarymonth)
    if (this.salarymonth == 'AUG') {
      const base_URL = 'http://localhost:8996/updateData/August'

      this.http.post(base_URL, {
        id: selected_id,
        empname: this.empname,
        monthlysalary: this.monthlysalarys,
        sickleave: this.sickleave,
        casualleave: this.casualleaves,
        dateofpayment: this.dateofpayment,
        monthlydays: this.monthlydays,
        workingdays: this.workingDays,
        outcomesalary: finalamount,
      }).subscribe(data => {
        console.log(base_URL)
        console.log(selected_id, this.empname, this.monthlysalary, this.monthlydays, this.workingDays)
        console.log(data['status'])
        if (data['status'] == '00') {
         this.deleteFieldValue(event)
          alert("Details has been Added Pushed to PaySlips !!!!")
        }else{
          alert("Something Went Wrong")
          window.location.reload()
        }

      })
    } else if (this.salarymonth == 'SEP') {
      const base_URL = 'http://localhost:8996/updateData/september'

      this.http.post(base_URL, {
        id: selected_id,
        empname: this.empname,
        monthlysalary: this.monthlysalarys,
        sickleave: this.sickleave,
        casualleave: this.casualleaves,
        dateofpayment: this.dateofpayment,
        monthlydays: this.monthlydays,
        workingdays: this.workingDays,
        outcomesalary: finalamount,
      }).subscribe(data => {
        console.log(base_URL)
        console.log(selected_id, this.empname, this.monthlysalary, this.monthlydays, this.workingDays)
        console.log(data['status'])
        if (data['status'] == '00') {

          alert("Details has been Added Pushed to PaySlips !!!!")
          window.location.reload();
        }

      })
    } else if (this.salarymonth == 'OCT') {
      const base_URL = 'http://localhost:8996/updateData/october'

      this.http.post(base_URL, {
        id: selected_id,
        empname: this.empname,
        monthlysalary: this.monthlysalarys,
        sickleave: this.sickleave,
        casualleave: this.casualleaves,
        dateofpayment: this.dateofpayment,
        monthlydays: this.monthlydays,
        workingdays: this.workingDays,
        outcomesalary: finalamount,
      }).subscribe(data => {
        console.log(base_URL)
        console.log(selected_id, this.empname, this.monthlysalary, this.monthlydays, this.workingDays)
        console.log(data['status'])
        if (data['status'] == '00') {

          alert("Details has been Added Pushed to PaySlips !!!!")
          window.location.reload();
        }

      })
    } else if (this.salarymonth == 'NOV') {
      const base_URL = 'http://localhost:8996/updateData/november'

      this.http.post(base_URL, {
        id: selected_id,
        empname: this.empname,
        monthlysalary: this.monthlysalarys,
        sickleave: this.sickleave,
        casualleave: this.casualleaves,
        dateofpayment: this.dateofpayment,
        monthlydays: this.monthlydays,
        workingdays: this.workingDays,
        outcomesalary: finalamount,
      }).subscribe(data => {
        console.log(base_URL)
        console.log(selected_id, this.empname, this.monthlysalary, this.monthlydays, this.workingDays)
        console.log(data['status'])
        if (data['status'] == '00') {

          alert("Details has been Added Pushed to PaySlips !!!!")
          window.location.reload();
        }

      })
    } else if (this.salarymonth == 'DEC') {
      const base_URL = 'http://localhost:8996/updateData/december'

      this.http.post(base_URL, {
        id: selected_id,
        empname: this.empname,
        monthlysalary: this.monthlysalarys,
        sickleave: this.sickleave,
        casualleave: this.casualleaves,
        dateofpayment: this.dateofpayment,
        monthlydays: this.monthlydays,
        workingdays: this.workingDays,
        outcomesalary: finalamount,
      }).subscribe(data => {
        console.log(base_URL)
        console.log(selected_id, this.empname, this.monthlysalary, this.monthlydays, this.workingDays)
        console.log(data['status'])
        if (data['status'] == '00') {

          alert("Details has been Added Pushed to PaySlips !!!!")
          window.location.reload();
        }

      })
    } else {
      alert("Something went Wrong")
    }

  }
}

