import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salary-list',
  templateUrl: './salary-list.component.html',
  styleUrls: ['./salary-list.component.less']
})
export class SalaryListComponent implements OnInit {
 
  employeedetailsList: any = []
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getAllList(event)
  }
  getAllList(event) {
    const base_URL = 'http://localhost:8996/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are you sure you want to delete the Data??")
    const base_URL = 'http://localhost:8996/delete/' + selected_id
    this.http.post(base_URL, {}).subscribe(data => {
      console.log(data)
      if (data['status'] == '00') {
        alert("Data has been sucessfully deleted")
        window.location.reload();
      }
    })
  }
}
