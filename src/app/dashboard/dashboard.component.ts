import { Component, OnInit, ViewChild } from '@angular/core';
import { Color } from 'ng2-charts';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent  implements OnInit{

  employeedata:any
  todocountData:any
  companyData:any

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getemployeeData(event)
    this.getToDoCount(event)
    this.getCompanyData(event)
   
  }
  public pieChartLabels = ['Employee-Count', 'Company-Count', 'To-Do Data-Count'];
  public pieChartData = [20, 40, 60];
  public pieChartType = 'pie';
  public pieChartColors: Color[] = [
    {backgroundColor:["#84DE02","#5E8C31","#FFDB00"]},

  ];

  public doughnutChartLabels = ['Employee-Count', 'Company-Count', 'To-Do Data-Count'];
  public doughnutChartData = [20, 40, 60];
  public doughnutChartType = 'doughnut';
  public doughnutChartColors: Color[] = [
    {backgroundColor:["#CCFF00","#66FF66","#00468C"]},
  
  ];
  
  getemployeeData(event){
    const base_url = "http://localhost:8990/count"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      
     this.employeedata = data;
     console.log(this.employeedata)
    })
  }
  getCompanyData(event){
    const base_url = "http://localhost:8992/count"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      console.log(data)
      this.companyData= data
    })
  }
  getToDoCount(event){
    const base_url = "http://localhost:8991/dataCount"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      console.log(data)
      this.todocountData = data
    })
  }

}
