import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';

import * as $ from "jquery"
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { HttpClientModule } from '@angular/common/http';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { SalaryManagementComponent } from './salary-management/salary-management.component';
import { SliderComponent } from './slider/slider.component';
import { SalaryListComponent } from './salary-list/salary-list.component';
import { MonthlysalaryComponent } from './monthlysalary/monthlysalary.component';
import { ViewpayslipComponent } from './viewpayslip/viewpayslip.component';
import { PayslipAugustComponent } from './payslip-august/payslip-august.component';
import { PrintpayslipComponent } from './printpayslip/printpayslip.component';
import { UploaddocComponent } from './uploaddoc/uploaddoc.component';
import { FileuploadViewComponent } from './fileupload-view/fileupload-view.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    
    EmployeedetailsComponent,
    ToDoListComponent,
    SalaryManagementComponent,
    SliderComponent,
    SalaryListComponent,
    MonthlysalaryComponent,
    ViewpayslipComponent,
    PayslipAugustComponent,
    PrintpayslipComponent,
    UploaddocComponent,
    FileuploadViewComponent,
    DashboardComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    Ng2SearchPipeModule,
    ChartsModule,
    Ng2OrderModule,
    NgxPaginationModule,
    HttpClientModule,
  FormsModule,
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})

export class AppModule { }