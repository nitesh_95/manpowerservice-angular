import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payslip-august',
  templateUrl: './payslip-august.component.html',
  styleUrls: ['./payslip-august.component.less']
})
export class PayslipAugustComponent implements OnInit {

  employeeDataAugust:any = []
  employeeDataAugustSeptember:any = []
  employeeDataAugustOctober:any = []
  employeeDataAugustNovember:any = []
  employeeDataAugustDecember:any = []
  constructor() { }
  pageSize:any = 10
  ngOnInit() {
    this.employeeDataAugust = JSON.parse(sessionStorage.getItem('employeeDataAugust'))
    this.employeeDataAugustSeptember = JSON.parse(sessionStorage.getItem('employeeDataSeptember'))
    this.employeeDataAugustOctober = JSON.parse(sessionStorage.getItem('employeeDataOctober'))
    this.employeeDataAugustNovember = JSON.parse(sessionStorage.getItem('employeeDataNovember'))
    this.employeeDataAugustDecember = JSON.parse(sessionStorage.getItem('employeeDataDecember'))
  }
}
