import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayslipAugustComponent } from './payslip-august.component';

describe('PayslipAugustComponent', () => {
  let component: PayslipAugustComponent;
  let fixture: ComponentFixture<PayslipAugustComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayslipAugustComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayslipAugustComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
