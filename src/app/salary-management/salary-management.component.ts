import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salary-management',
  templateUrl: './salary-management.component.html',
  styleUrls: ['./salary-management.component.less']
})
export class SalaryManagementComponent implements OnInit {
names:any
adhars:any
pans:any
mobiles:any
addresses:any
id:any
empname:any
adhar:any
salary:any
month:any
status:any
monthlysalary:any
joineddate:any
joineddates:any
employeedetailsList:any = []
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getAllList(event)
  }
  getAllList(event) {

    const base_URL = 'http://localhost:8990/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {

      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
  editedDetails(event){
    var selected_id = event.currentTarget.id
    const base_URL = 'http://localhost:8990/getEmployee/'+ selected_id
      this.http.post(base_URL, {
      }).subscribe(data => {
        this.id=data['id']
        this.names =data['name']
        this.adhars=data['adhar']
        this.pans=data['pan']
        this.mobiles=data['mobile']
        this.addresses=data['address']
        this.joineddates=data['joineddate']
  
      })
    }
    // editedDetails(event) {
    //   const base_URL = 'http://localhost:8990/updateData'
    //   this.http.post(base_URL, {
    //     id:this.id,
    //     empname: this.empname,
    //     adhar: this.adhar,
    //     salary: this.salary,
    //     month:this.month
    //   }).subscribe(data => {
    //     console.log(data['status'])
    //     if (data['status'] == '00') {
    //       alert("Employee Salary has been Added Successfully")
    //       window.location.reload();
    //     }
  
    //   })
    // }
    clickSubmit(event) {
      // this.status = (<HTMLInputElement>document.getElementById("status")).value;
      const base_URL = 'http://localhost:8996/updateData'
      this.http.post(base_URL, {
        id:this.id,
        empname: this.names,
        adhar: this.adhars,
        joineddate: this.joineddates,
        monthlysalary:this.monthlysalary
      }).subscribe(data => {
        console.log(data['status'])
        if (data['status'] == '00') {
          alert("Employee Salary has been Added Successfully")
          window.location.reload();
        }
    
      })
    }
}
