import { Component, OnInit } from '@angular/core';

import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  
  title = ' BHUSHAN MANAGEMENT SYSTEM';
  storenm : string;
  showMsg : boolean;
  disableLogin = false;
  showHead = true
  showslider = true
  showfooter = true;
  constructor(private router: Router) {

 
   
  
      router.events.forEach((event) => {
        if (event instanceof NavigationStart) {
          if (event['url'] == '/login') {
            this.showHead = false;
            this.showslider = false;
            this.showfooter = false;
          } else {
            this.showHead = true;
            this.showslider = true;
            this.showfooter = true;
          }
        }
      });
  
    }

  //get store name
  
  setLoginDisabled()
  {
    this.disableLogin = true;
  }
}

