import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({

  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']

})

export class HomeComponent implements OnInit {
compname:any = []
employeedetailsList:any= []
ngOnInit(){
this.getAllCompName(event)
this.getAllEmployee(event)
}
constructor(private http:HttpClient,private router:Router){}
getAllCompName(event){
  const base_URL = 'http://localhost:8992/getAll'
  this.http.post(base_URL, {
  }).subscribe(data => {

    this.compname.push(data)
    this.compname = this.compname[0]
    console.log(data)

  })
}
getAllEmployee(event){
  
  const base_URL = 'http://localhost:8990/getAll'
  this.http.post(base_URL, {
  }).subscribe(data => {

    this.employeedetailsList.push(data)
    this.employeedetailsList = this.employeedetailsList[0]
    console.log(data)

  })
}
}