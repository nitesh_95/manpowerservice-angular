import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { saveAs } from "file-saver";

@Component({
  selector: 'app-uploaddoc',
  templateUrl: './uploaddoc.component.html',
  styleUrls: ['./uploaddoc.component.less']
})
export class UploaddocComponent implements OnInit {
  id: any
  ids: any
  idss: any
  myFiles: string[] = [];
  constructor(private httpClient: HttpClient, private router: Router) { }
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  uri: any
  filename: any
  base_url;
  ngOnInit() {

  }

  onFileChange(event) {

    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
      let files = this.myFiles[i]

      const uploadImageData = new FormData();
      uploadImageData.append("files", files);
      this.httpClient.post('http://localhost:8993/uploadMultipleFiles', uploadImageData)
        .subscribe((data) => {
          console.log(data)
         alert("Document has been Uploaded Sucessfully")
        })
      }
    }
    downloadPdf(event) {
    const base_url = 'http://localhost:8993/downloadFile/6b2f242b-92aa-4868-9a52-971dec97f581'
        this.httpClient.get(base_url, {responseType: "blob", headers: {'Accept': ".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf"}})
        .subscribe(blob => {
          saveAs(blob,'sample.jpg');
        });
      }
    }
    
    